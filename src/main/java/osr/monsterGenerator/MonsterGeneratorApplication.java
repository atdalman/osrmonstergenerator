package osr.monsterGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;
import osr.monsterGenerator.npc.BaseNPC;
import osr.monsterGenerator.service.AttributeService;
import osr.monsterGenerator.service.NPCFactory;
import osr.monsterGenerator.utilities.Systems;

@SpringBootApplication
public class MonsterGeneratorApplication implements CommandLineRunner {

	@Autowired
	private NPCFactory npcFactory;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private AttributeService attributeService;

	public static void main(String[] args) {
		SpringApplication.run(MonsterGeneratorApplication.class, args);
	}

	@Override
	public void run(String... args) {
		BaseNPC firstDndNPC = npcFactory.generateNPC(Systems.BASENPC);
		System.out.println(firstDndNPC.toString());
//		mongoTemplate.remove(new Query(), "size");
//		Size surface = new Size();
//		for (Sizes curr : Sizes.values()) {
//			surface = new Size();
//			surface.setName(curr.name().toLowerCase());
//			surface.setChance(curr.weight);
//			mongoTemplate.insert(surface);
//		}
	}

}