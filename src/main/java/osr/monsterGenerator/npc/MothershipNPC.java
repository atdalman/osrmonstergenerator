package osr.monsterGenerator.npc;

public class MothershipNPC extends BaseNPC {
    public String instinct;
    public String combat;
    // Used for mercenaries
    public String loyalty;

    public String getInstinct() {
        return instinct;
    }

    public void setInstinct(String instinct) {
        this.instinct = instinct;
    }

    public String getCombat() {
        return combat;
    }

    public void setCombat(String combat) {
        this.combat = combat;
    }

    public String getLoyalty() {
        return loyalty;
    }

    public void setLoyalty(String loyalty) {
        this.loyalty = loyalty;
    }
}
