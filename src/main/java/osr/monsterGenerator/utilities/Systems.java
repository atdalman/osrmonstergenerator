package osr.monsterGenerator.utilities;

public enum Systems {

    ASSH, //Astonishing Swordsmen & Sorcerers of Hyperborea
    MOTHERSHIP,
    DND5E,
    OSROTHER,
    BASENPC;

}
